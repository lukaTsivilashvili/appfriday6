package com.example.appfriday5

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.example.appfriday5.databinding.InsertViewInfoBinding

class ViewInfoFragment : Fragment() {

    private lateinit var binding: InsertViewInfoBinding
    private val viewModel: InsertViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = InsertViewInfoBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init() {

        observes()

        binding.saveBtn.setOnClickListener {
            viewModel.write(
                binding.ETtitle.text.toString(),
                binding.ETdescription.text.toString(),
                binding.ETphotoUrl.text.toString()
            )

            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.mainRecyclerFragment) }
        }


    }

    private fun observes() {
        viewModel.users.observe(viewLifecycleOwner, {
            Log.d("myMessage", "${it.size}")
        })
    }

}