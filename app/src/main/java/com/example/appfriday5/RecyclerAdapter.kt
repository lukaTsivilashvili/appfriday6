package com.example.appfriday5

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.appfriday5.databinding.ItemRecyclerBinding
import com.example.appfriday5.roomstuff.UserModel

class RecyclerAdapter(private val context:Context, private var infos: List<UserModel>):RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    inner class ViewHolder(private val binding:ItemRecyclerBinding):RecyclerView.ViewHolder(binding.root){
        private lateinit var model: UserModel
        fun bind() {
            model = infos[adapterPosition]

            binding.TVtitle.text = model.title
            binding.TVdescription.text = model.description

            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transform(CenterCrop(), RoundedCorners(16))

            Glide.with(context)
                .load(model.url)
                .placeholder(R.drawable.ic_launcher_background)
                .apply(requestOptions)
                .into(binding.imageView)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView =
            ItemRecyclerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = ViewHolder(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.bind()
    }

    override fun getItemCount() = infos.size

}