package com.example.appfriday5.roomstuff

import androidx.room.Room
import com.example.appfriday5.MyApp

object Database {

    val db = Room.databaseBuilder(
        MyApp.instance.applicationContext,
        AppDataBase.AppDatabase::class.java, "user"
    ).build()
}