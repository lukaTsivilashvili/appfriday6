package com.example.appfriday5.roomstuff

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserModel(
    @PrimaryKey(autoGenerate = true) val uid: Int = 0,
    @ColumnInfo(name = "title") val title: String?,
    @ColumnInfo(name = "description") val description: String?,
    @ColumnInfo(name = "photourl") val url: String
) {
    constructor(
        title: String?,
        description: String?,
        photourl: String
    ) : this(0, title, description, photourl)
}
