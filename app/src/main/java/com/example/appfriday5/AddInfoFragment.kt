package com.example.appfriday5

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.appfriday5.databinding.InfoFragmentBinding

class AddInfoFragment : Fragment() {

    private lateinit var binding: InfoFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = InfoFragmentBinding.inflate(inflater, container, false)
        btnClick()
        return binding.root
    }

    private fun btnClick() {

        binding.addBtn.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.viewInfoFragment) }
        }

    }


}