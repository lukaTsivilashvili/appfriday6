package com.example.appfriday5.roomstuff

import androidx.room.Database
import androidx.room.RoomDatabase

class AppDataBase {

    @Database(entities = arrayOf(UserModel::class), version = 1)
    abstract class AppDatabase : RoomDatabase() {
        abstract fun userDao(): UserDao
    }

}