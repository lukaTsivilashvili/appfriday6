package com.example.appfriday5

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.appfriday5.roomstuff.Database
import com.example.appfriday5.roomstuff.UserModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class InsertViewModel:ViewModel() {

    private val _users = MutableLiveData<List<UserModel>>()

    val users: LiveData<List<UserModel>> = _users

    fun read() {

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _users.postValue(Database.db.userDao().getAll())
            }
        }

    }

    fun write(
        title: String,
        description: String,
        photourl: String
    ) {

        val users = UserModel(title, description, photourl)

        viewModelScope.launch {
            withContext(Dispatchers.IO){
                Database.db.userDao().insertAll(users)
            }
        }


    }

}