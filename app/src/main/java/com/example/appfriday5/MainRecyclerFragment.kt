package com.example.appfriday5

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appfriday5.databinding.FragmentMainRecyclerBinding


class MainRecyclerFragment : Fragment() {

    private lateinit var binding: FragmentMainRecyclerBinding
    private lateinit var myAdapter: RecyclerAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentMainRecyclerBinding.inflate(inflater, container, false)
        initRecycler()
        return binding.root
    }

    private fun initRecycler() {
        val model = ViewModelProvider(requireActivity()).get(InsertViewModel::class.java)
        val allData = model.read()

        myAdapter = RecyclerAdapter(requireContext(), allData)
        binding.recycler.layoutManager = LinearLayoutManager(requireActivity())
        binding.recycler.adapter = myAdapter
    }


}